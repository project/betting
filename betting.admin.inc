<?php
/**
* (C) 2009 Petr 'kecinzer' Řezníček
*/

define('MAX_ABONENTS', 50);

function betting_special_positions() {
  return array(
    -11 => t('Win the @pos abonent', array('@pos' => t('first'))),
    -10 => t('Draw'),
    -12 => t('Win the @pos abonent', array('@pos' => t('second'))),
    -110 => t('Win the @pos abonent in prolongation', array('@pos' => t('first'))),
    -102 => t('Win the @pos abonent in prolongation', array('@pos' => t('second'))),
  );
}

function betting_admin_eventuality($form_state) {
  if ($form_state['values']['op'] == t('Delete selected')) {
    return betting_admin_eventuality_confirm($form_state, array_filter($form_state['values']['eids']));
  }
  $form = betting_admin_eventuality_list();
  
  return $form;
}

function betting_admin_eventuality_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete selected')) {
    $eids = array_filter($form_state['values']['eids']);
    if (count($eids) == 0) {
      form_set_error('', t('No items selected.'));
    }
  }
}

function betting_admin_eventuality_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete selected')) {
    $form_state['rebuild'] = TRUE;
  }
  else {
    foreach ($form_state['values']['eids'] as $eid => $to_delete) {
      $special = ($form_state['values']['specials'][$eid]) ? 1 : 0;
      db_query('UPDATE {betting_eventuality} SET abonents = %d, special = %d, weight = %d WHERE eid = %d', $form_state['values']['abonents'][$eid], $special, $form_state['values']['weight'][$eid], $eid);
    }
    drupal_set_message(t('Eventualities has been updated.'));
  }
}

function betting_admin_eventuality_confirm(&$form_state, $eids) {
  $form['eids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($eids as $eid => $value) {
    $name = db_result(db_query('SELECT description FROM {betting_eventuality} WHERE eid = %d', $eid));
    $form['eids'][$eid] = array(
      '#type' => 'hidden',
      '#value' => $eid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'betting_admin_eventuality_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/eventuality', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function betting_admin_eventuality_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['eids'] as $eid => $value) {
      db_query('DELETE FROM {betting_eventuality} WHERE eid = %d', $eid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/eventuality';
  return;
}

function betting_admin_eventuality_list() {
  for ($i = 2; $i <= MAX_ABONENTS; $i++) {
    $abonents[$i] = $i;
  }
  $eventuality = betting_get_eventualities();
  
  foreach ($eventuality as $row) {
    $eids[$row->eid] = '';
    if ($row->special) {
      $specials[] = $row->eid;
    }
    $form['abonents']['#tree'] = TRUE;
    $form['weight']['#tree'] = TRUE;    
    $form['result'][$row->eid] = array('#value' => $row->result);
    $form['description'][$row->eid] = array('#value' => $row->description);
    $form['abonents'][$row->eid] = array('#type' => 'select', '#default_value' => $row->abonents, '#options' => $abonents);
    $form['weight'][$row->eid] = array('#type' => 'weight', '#default_value' => $row->weight, '#delta' => 50);
    $form['edit'][$row->eid] = array('#value' => l(t('edit'), 'admin/betting/eventuality/edit/' . $row->eid));
  }  
  if (is_array($eids)) {
    $form['eids'] = array(
      '#type' => 'checkboxes',
      '#options' => $eids,
    );
    $form['specials'] = array(
      '#type' => 'checkboxes',
      '#options' => $eids,
      '#default_value' => $specials ? $specials : array(),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected'),
  );
  return $form;
}

function theme_betting_admin_eventuality($form) {
  $header = array(
    '',
    t('Result'),
    t('Description'),
    t('Abonents'),
    t('Special'),
    t('Weight'),
    t('Operations'),
    t('Delete'),
  );
  drupal_add_tabledrag('betting-eventuality', 'order', 'sibling', 'eventuality-weight', 'eventuality-weight-2', NULL, FALSE);
  drupal_add_tabledrag('betting-eventuality', 'order', 'sibling', 'eventuality-weight', 'eventuality-weight-more', NULL, FALSE);
  if (isset($form['result']) && is_array($form['result'])) {
    foreach (element_children($form['result']) as $key) {
      if ($form['abonents'][$key]['#value'] == 2 && !$two_abonents) {
        $row = array('data' => t('2 abonents'), 'colspan' => 8, 'class' => 'region');
        $rows[] = array('data' => array($row), 'class' => 'region');      
        $two_abonents = TRUE;
        $abonents = '2';
      }     
      if ($form['abonents'][$key]['#value'] > 2 && !$more_abonents) {
        $row = array('data' => t('More than 2 abonents'), 'colspan' => 8, 'class' => 'region');
        $rows[] = array('data' => array($row), 'class' => 'region');
        $more_abonents = TRUE;
        $abonents = 'more';
      }
      $form['weight'][$key]['#attributes']['class'] = 'eventuality-weight eventuality-weight-' . $abonents;
      $form['abonents'][$key]['#attributes']['class'] = 'eventuality-abonents eventuality-abonents-' . $abonents;
      $row = array(
        array(''),
        drupal_render($form['result'][$key]),
        drupal_render($form['description'][$key]),
        drupal_render($form['abonents'][$key]),
        drupal_render($form['specials'][$key]),
        drupal_render($form['weight'][$key]),
        drupal_render($form['edit'][$key]),
        drupal_render($form['eids'][$key]),
      );
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  $output = theme('table', $header, $rows, array('id' => 'betting-eventuality'));
  $output .= drupal_render($form);
  
  return $output;
}

/**
* Přidání eventuality - formulář
* 
* @param mixed $form_state
*/
function betting_admin_eventuality_add($form_state, $eventuality = NULL) {
  if ($eventuality->eid) {
    $form['eid'] = array(
      '#type' => 'hidden',
      '#value' => $eventuality->eid,
    );
  }
  $form['result'] = array(
    '#type' => 'textfield',
    '#title' => t('Result'),
    '#default_value' => $eventuality->result,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $eventuality->description,
    '#required' => TRUE,
  );
  for ($i = 2; $i <= MAX_ABONENTS; $i++) {
    $abonents[$i] = $i;
  }  
  $form['abonents'] = array(
    '#type' => 'select',
    '#title' => t('Minimum of abonents'),
    '#options' => $abonents,
    '#default_value' => $eventuality->abonents,
  );
  $form['special'] = array(
    '#type' => 'checkbox',
    '#title' => t('Special'),
    '#default_value' => $eventuality->special,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $eventuality->weight,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function betting_admin_eventuality_add_submit($form, &$form_state) {
  if ($form_state['values']['eid']) {
    db_query('UPDATE {betting_eventuality} SET abonents = %d, result = "%s", description = "%s", special = %d, weight = %d WHERE eid = %d',
      $form_state['values']['abonents'], $form_state['values']['result'], $form_state['values']['description'], $form_state['values']['special'], $form_state['values']['weight'], $form_state['values']['eid']);
    drupal_set_message(t('Eventuality has been updated.'));
  }
  else {
    db_query('INSERT INTO {betting_eventuality} (abonents, result, description, special, weight) VALUES (%d, "%s", "%s", %d, %d)',
      $form_state['values']['abonents'], $form_state['values']['result'], $form_state['values']['description'], $form_state['values']['special'], $form_state['values']['weight']);
    drupal_set_message(t('Eventuality has been created.'));
  }
  $form_state['redirect'] = 'admin/betting/eventuality';
}

function betting_admin_assignment($form_state) {
  if ($form_state['values']['delete'] == t('Delete selected')) {
    return betting_admin_assignment_confirm($form_state, array_filter($form_state['values']['poss']));
  }
  $form = betting_admin_assignment_list();
  
  return $form;
}

function betting_admin_assignment_validate($form, &$form_state) {
  if (!array_filter($form_state['values']['poss'])) {
    form_set_error('', t('No items selected.'));
  }
}

function betting_admin_assignment_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function betting_admin_assignment_confirm(&$form_state, $poss) {
  $form['poss'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($poss as $pos => $value) {
    $form['poss'][$pos] = array(
      '#type' => 'hidden',
      '#value' => $pos,
      '#prefix' => '<li>',
      '#suffix' => check_plain($pos) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'betting_admin_assignment_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/assignment', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function betting_admin_assignment_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['poss'] as $pos => $value) {
      db_query('DELETE FROM {betting_assignment} WHERE pos = %d', $pos);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/assignment';
  return;
}

/**
* Přiřazování eventualit k správnému pořadí
* 
* @param mixed $form_state
*/
function betting_admin_assignment_list() {
  $POSITION = betting_special_positions();  
  $sql = 'SELECT a.pos, e.result, e.description FROM {betting_assignment} a INNER JOIN {betting_eventuality} e ON a.eid = e.eid ORDER BY a.pos';
  $result = db_query($sql);
  while ($row = db_fetch_object($result)) {
    if (!$form['pos'][$row->pos]) {
      $poss[$row->pos] = '';
      $form['pos'][$row->pos] = array('#value' => $row->pos . ($POSITION[$row->pos] ? ' (' . $POSITION[$row->pos] . ')' : ''));
      $form['edit'][$row->pos] = array('#value' => l(t('edit'), 'admin/betting/assignment/edit/'.$row->pos));
    }
    $eventuality[$row->pos][] = $row->result . ' (' . $row->description . ')';    
  }
  if ($poss) {
    $form['poss'] = array(
      '#type' => 'checkboxes',
      '#options' => $poss
    );
    foreach ($eventuality as $key => $value) {
      $form['eventuality'][$key] = array('#value' => theme('item_list', $value));
    }
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    ); 
  }
  
  return $form;
}

function theme_betting_admin_assignment($form) {
  $header = array(
    t('Position'),
    t('Eventuality'),
    t('Edit'),
    t('Delete'),
  );
  if (isset($form['pos']) && is_array($form['pos'])) {
    foreach (element_children($form['pos']) as $key) {
      $rows[] = array(
        drupal_render($form['pos'][$key]),
        drupal_render($form['eventuality'][$key]),
        drupal_render($form['edit'][$key]),
        drupal_render($form['poss'][$key]),
      );
    }
  }  
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

function betting_admin_assignment_form($form_state, $edit = NULL) {  
  $positions[t('For 2 abonents')] = betting_special_positions();
  $positions[t('For more abonents')][999] = '999 - ' . t('Not finished');
  $positions[t('For more abonents')][0] = '0 - ' . t('End position');
  for($i = 1; $i <= MAX_ABONENTS; $i++) {
    $positions[t('For more abonents')][$i] = $i;
  }  
  $form['pos'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#description' => t('Position 0 means last position for any number of abonents.'),
    '#options' => $positions,
    '#default_value' => $edit->pos,
  );
  foreach (betting_get_eventualities() as $value) {
    $eventualities[$value->eid] = $value->result . ' (' . $value->description . ')';
  }  
  $form['eventuality'] = array(
    '#type' => 'select',
    '#title' => t('Eventuality'),
    '#description' => t('Select all eventualities that belongs selected position.'),
    '#multiple' => TRUE,
    '#size' => 25,
    '#options' => $eventualities,
    '#default_value' => $edit->eids, 
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );  
  return $form;
}

/**
* Uložení do DB
* 
* @param mixed $form
* @param mixed $form_state
*/
function betting_admin_assignment_form_submit($form, &$form_state) {
  db_query('DELETE FROM {betting_assignment} WHERE pos = %d', $form_state['values']['pos']);
  foreach ($form_state['values']['eventuality'] as $value) {
    db_query('INSERT INTO {betting_assignment} (pos, eid) VALUES (%d, %d)', $form_state['values']['pos'], $value);    
  }
  drupal_set_message(t('Assignment has been updated.'));
  $form_state['redirect'] = 'admin/betting/assignment';
}

function betting_add_course($form_state, $course = NULL) {
  $form['course-title'] = array(
    '#type' => 'item',
    '#title' => t('New course'),
  );
  $form['course'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#prefix' => '<div class="container-inline">',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => '</div>',
  );                        
  $form['nodetype'] = array(
    '#type' => 'hidden',
    '#value' => $course->type,
  );
  $form['archiv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Old courses'),
    '#theme' => 'bettingflexi_add_course_table',
  );
  foreach ($course->list as $item) {
    $form['archiv'][$item->time]['time'] = array(
      '#type' => 'item',
      '#value' => format_date($item->time, 'custom', 'd.m.Y, H:i:s'),
    );
    $form['archiv'][$item->time]['course'] = array(
      '#type' => 'item',
      '#value' => $item->course,
    );
  }
  
  return $form;
}

function betting_add_course_validate($form, &$form_state) {
  $new_course = betting_float($form_state['values']['course']);
  if (!is_numeric($new_course)) {
    form_set_error('course', t('Course has to be a number.'));
  }
  elseif ($new_course < 0) {
    form_set_error('course', t('Course has to be a number greater than zero.'));
  }
}

function betting_add_course_submit($form, &$form_state) {
  // ještě poslední hook
  module_invoke($form_state['values']['nodetype'], 'course_insert_single', $form_state['values']['course']);
  drupal_set_message(t('Course has been created.'));
}

function game_course_insert_single($course) {
  $arg = explode('-', arg(2));
  db_query('INSERT INTO {betting_courses} (nid, eid, aid, time, course) VALUES (%d, %d, %d, %d, %f)', arg(1), $arg['0'], $arg['1'], time(), betting_float($course));
}

function theme_betting_add_course_table($form) {
  $header = array(
    t('Time'),
    t('Course'),
  );  
  foreach (element_children($form) as $time) {
    $rows[] = array(
      drupal_render($form[$time]['time']),
      drupal_render($form[$time]['course']),
    );
  }
  $output = theme('table', $header, $rows, array('class' => 'betting-edit'));
  $output .= drupal_render($form);
  return $output;  
}