<?php
/**
* (c) 2009 Petr 'kecinzer' Řezníček
*/

function bettingflexi_admin_eventuality($form_state) {
  if ($form_state['values']['delete'] == t('Delete selected')) {
    return bettingflexi_admin_eventuality_confirm($form_state, array_filter($form_state['values']['eidfs']));
  }
  $form = bettingflexi_admin_eventuality_list();
  
  return $form;
}

function bettingflexi_admin_eventuality_validate($form, &$form_state) {
  $eidfs = array_filter($form_state['values']['eidfs']);
  if (count($eidfs) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

function bettingflexi_admin_eventuality_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bettingflexi_admin_eventuality_confirm(&$form_state, $eidfs) {
  $form['eidfs'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($eidfs as $eidf => $value) {
    $name = db_result(db_query('SELECT result FROM {bettingflexi_eventuality} WHERE eidf = %d', $eidf));
    $form['eidfs'][$eidf] = array(
      '#type' => 'hidden',
      '#value' => $eidf,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingflexi_admin_eventuality_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/eventuality-flexi', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function bettingflexi_admin_eventuality_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['eidfs'] as $eidf => $value) {
      db_query('DELETE FROM {bettingflexi_eventuality} WHERE eidf = %d', $eidf);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/eventuality-flexi';
  return;
}

/**
* Editace flexibilních eventualit
* 
* @param mixed $form_state
*/
function bettingflexi_admin_eventuality_list() {
  // sem chci ještě přidat stránkování - bude jich fakt hodně
  $result = pager_query('SELECT * FROM {bettingflexi_eventuality} ORDER BY eidf DESC', 25);
  while ($row = db_fetch_object($result)) {
    $cid = db_result(db_query('SELECT cid FROM {bettingflexi_courses} WHERE eidf = %d LIMIT 1', $row->eidf));
    if (!$cid) {
      $eidfs[$row->eidf] = '';
    }
    $form['no'][$row->eidf] = array('#value' => $row->eidf);
    $form['result'][$row->eidf] = array('#value' => $row->result);
    $form['edit'][$row->eidf] = array('#value' => l(t('edit'), 'admin/betting/eventuality-flexi/'.$row->eidf));
  }
  if (is_array($eidfs)) {
    $form['eidfs'] = array(
      '#type' => 'checkboxes',
      '#options' => $eidfs,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  if (is_array($form['result'])) {
    $form['pager'] = array('#value' => theme('pager', NULL, 25));
  }
  return $form;
}

function theme_bettingflexi_admin_eventuality(&$form) {
  $header = array(
    '',
    t('Result'),
    t('Operations'),
    t('Delete'),
  );
  if (isset($form['result']) && is_array($form['result'])) {
    foreach (element_children($form['result']) as $key) {
      $rows[] = array(
        drupal_render($form['no'][$key]),
        drupal_render($form['result'][$key]),
        drupal_render($form['edit'][$key]),
        drupal_render($form['eidfs'][$key]),
      );
    }
  }
  $output = theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  
  return $output;
}

function bettingflexi_admin_eventuality_edit($form_state, $eflexi) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Eventuality name',
    '#default_value' => $eflexi->result,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function bettingflexi_admin_eventuality_edit_submit($form, &$form_state) {
  db_query('UPDATE {bettingflexi_eventuality} SET result = "%s" WHERE eidf = %d', $form_state['values']['name'], arg(3));
  drupal_set_message(t('Flexible eventuality have been updated.'));
  $form_state['redirect'] = 'admin/betting/eventuality-flexi';
}

/**
* Modul Betting System a všechny jeho součásti jsou majetkem Petra Řezníčka (posta@petrreznicek.cz).
* Jakékoliv úpravy a další používání bez souhlasu autora budou považovány za porušení zákona č. 121/2000 Sb. 
* (C) 2009 Petr 'kecinzer' Řezníček
*/