<?php
/**
* (c) 2009 Petr 'kecinzer' Řezníček
*/

function bettingrelation_help($path, $arg) {
  switch ($path) {
    case 'admin/help#bettingrelation':
      $output = '<p>' . t('Betting system relation module adding possibility to define relation between games.') . '</p>';
      $output .= '<p>' . t('This can be used for creating parts of game (single matches for tournament or half or third).') . '</p>';
      $output .= '<p>' . t('Module don\'t have own administration. Relations id defined during game creation.') . '</p>';
      return $output;
  }
}

/**
* Implementace hook_to_ticket_access
* 
* @param mixed $nid
*/
function bettingrelation_to_ticket_access($nid) {
  global $user;
  // načíst rodiče, pokud existují
  $pnid = db_result(db_query('SELECT pnid FROM {bettingrelation} WHERE nid = %d', $nid));
  if ($user->ticket[$pnid]) {
    return FALSE;
  }
  // mám rodiče, ale ten může mít více potomků a jeden z nich může být označen...
  $result = db_query('SELECT nid FROM {bettingrelation} WHERE pnid = %d', $pnid);
  while ($row = db_fetch_object($result)) {
    if ($user->ticket[$row->nid]) {
      return FALSE;
    }
  }
  // načíst potomky pokud existují
  $result = db_query('SELECT nid FROM {bettingrelation} WHERE pnid = %d', $nid);
  while ($row = db_fetch_object($result)) {
    if ($user->ticket[$row->nid]) {
      return FALSE;
    }
  }
}

function bettingrelation_have_results($nid) {
  $pnid = db_result(db_query('SELECT pnid FROM {bettingrelation} WHERE nid = %d', $nid));
  if ($pnid) {
    // načtu počet výsledků z tabulky rodiče
    $results = db_result(db_query('SELECT COUNT(*) FROM {betting_results} WHERE nid = %d', $pnid));
    if ($results) {
      return TRUE;
    }
  }
}

/**
* Implementace hooku games_view_join_list
* 
* @param mixed $type
*/
function bettingrelation_games_view_join_list() {
  return 'LEFT JOIN {bettingrelation} rl ON n.nid = rl.nid';
}

/**
* Implementace hooku games_view_where_list
* 
* @param mixed $type
*/
function bettingrelation_games_view_where_list($type) {
  switch ($type) {
    case 'offer':
    case 'pending':
      return 'AND ISNULL(rl.nid)';
    case 'results':
      return 'AND ISNULL(rl.nid)';
  }
}

/**
* Implementeace hooku game_delete_conditions
* 
* @param mixed $nid
*/
function bettingrelation_game_delete_conditions($nid) {
  // pokud bude mít nějaké děti, nemohu ho smazat
  if ($has_childs = db_result(db_query('SELECT COUNT(nid) FROM {bettingrelation} WHERE pnid = %d', $nid))) {
    return t('Game can not be deleted, because have another games in relation.');
  }
}

/**
* Implementace hooku game_delete - odstraním dodatečná data
* 
* @param mixed $nid
*/
function bettingrelation_game_delete($nid) {
  db_query('DELETE FROM {bettingrelation} WHERE nid = %d', $nid);
}

/**
* Implementace hook_nodeapi
* 
* @param mixed $node
* @param mixed $op
* @param mixed $a3
* @param mixed $a4
*/
function bettingrelation_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if (betting_is_gametype($node->type)) {
    switch ($op) {
      // vložení do databáze
      case 'insert':
        if ($node->pnid) {
          db_query('INSERT INTO {bettingrelation} (pnid, nid) VALUES (%d, %d)', $node->pnid, $node->nid);  
        }
        break;
      // vymazání uzlu
      case 'delete':
      
        break;
      // kontrola zadaných údajů
      case 'validate':
        
        break;
      // těsně před uložením do DB nafejkuji falešný výběr kategorie (převzat od rodiče)
      // toto nakonec asi nebudu muset dělat, protože kategorie nastavím pomocí form_set_value ;)
      case 'presave':
        if ($node->pnid && !array_filter($node->taxonomy)) {
          $parent_node = node_load($node->pnid);
          foreach ($parent_node->taxonomy as $value) {
            $taxonomy[$value->vid] = $value->tid;   
          }
          $node->taxonomy = $taxonomy;
        }
        break;
      // načtení dodatečných dat do objektu $node
      case 'load':        
        // provádím právě editaci uzlu?
        $EDIT = (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit') ? TRUE : FALSE;
        // načtení ID rodiče (rodič může být jen jeden)
        $pnid = db_result(db_query('SELECT pnid FROM {bettingrelation} WHERE nid = %d', $node->nid));
        if ($pnid) {
          $node->pnid = $pnid;
          $p_title = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $pnid));
          if (!$EDIT) {
            // upravím titulek, aby bylo zřejmé, k čemu pod-hra náleží, zároveň musím někde uchovat původní titulek
            $node->title_pure = $node->title;
            $node->title = $p_title . ' (' . $node->title . ')';
          }
        }
        // načtení ID potomka - může jich mít vic
        $result = db_query('SELECT nid FROM {bettingrelation} WHERE pnid = %d', $node->nid);
        while ($row = db_fetch_object($result)) {
          $chnid[] = $row->nid;
        }
        if ($chnid) {
          // začnu načítat tabulku s výsledky
          $node->chnid = $chnid;
        }
        break;
      // zobrazení :)
      case 'view':
        if ($node->pnid && !array_filter($node->taxonomy)) {
          $parent_node = node_load($node->pnid);
          foreach ($parent_node->taxonomy as $value) {
            $taxonomy[$value->vid] = $value->tid;   
          }
          $node->taxonomy = $taxonomy;
        }
        if ($node->chnid) {
          $node->content['relation'] = array(
            '#title' => t('Other tips'),
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => ($a4 == 0) ? TRUE : FALSE,
            '#weight' => 8,
          );
          $childs = bettingrelation_display_childs($node->chnid);
          $node->content['relation'] = $node->content['relation'] + $childs;
        }
        break;
    }
  }
}

function bettingrelation_display_childs($childs) {
  $i = -10;
  foreach ($childs as $nid) {
    // načtu všechny informace o potomkovi
    $node = node_load($nid);
    $node->MESSAGE = betting_bet_access($node);    
    // zde musím volat hook v konkrétním modulu, podle toho co je to za typ hry (uzlu)
    // čili zde bude něco ve smyslu module_invoke() + zjištění typu uzlu    
    $CHILDREN[$nid] = array(
      '#value' => module_invoke($node->type, 'table_tips_view', $node, $node->title_pure, TRUE),
      '#weight' => $i,
    );
    if ($node->result) {
      $CHILDREN[$nid]['result'] = array(
        '#value' => module_invoke($node->type, 'table_results_view', $node),
      );
    }
    $i++;
  }
  return $CHILDREN;
}

/**
* Implementace hook_form_alter
* 
* @param mixed $form
* @param mixed $form_state
* @param mixed $form_id
*/
function bettingrelation_form_alter(&$form, $form_state, $form_id) {
  $type = preg_replace('/_node_form/', '', $form_id);
  if (betting_is_gametype($type)) {
    drupal_add_js(drupal_get_path('module', 'bettingrelation') . '/bettingrelation.js');
    // provádím editaci uzlu?
    $editing = isset($form['nid']['#value']);
    // přidám vlastní validační funkci pro možnost form_set_value ;)
    if (!$editing) {
      $form['#validate'][] = 'bettingrelation_validate_custom';
    }
    foreach (element_children($form['taxonomy']['betting']) as $vid) {
      // nastavit REQUIRED ručně
      $form['taxonomy']['betting'][$vid]['#required'] = FALSE;
      $form['taxonomy']['betting'][$vid]['#title'] = $form['taxonomy']['betting'][$vid]['#title'] . ' ' . _betting_required_value();
    }
    $form['game']['#attributes']['id'] = 'game-hiddable';
    // spražení ukazuji pouze pokud vytvářím hru, nebo pokud u ní již nějaké spřažení je
    if (!$editing && $form['#node']->step < 2 || $form['#node']->pnid) {
      $form['taxonomy']['betting']['#attributes']['id'] = 'taxonomy-hiddable';
      $form['relation'] = array(
        '#type' => 'fieldset',
        '#title' => t('Game relation'),
        '#description' => t('For creating parts of another game. After selecting parent game, abonents and other infos will be filled automaticaly.'),
        '#collapsible' => TRUE,
        '#collapsed' => $form['#node']->pnid ? FALSE : TRUE,
        '#attributes' => array('id' => 'relation-hiddable'),
        '#weight' => -5,
      );  
      // vybírám rodiče
      $form['relation']['pnid'] = array(
        '#type' => 'select',
        '#title' => t('Parent game'),
        '#options' => array('' => t('- None selected -')) + bettingrelation_get_games($editing),
        '#default_value' => $form['#node']->pnid,
        '#disabled' => ($form['#node']->pnid) ? TRUE : FALSE,
      ); 
    }
  }  
}

function bettingrelation_validate_custom(&$form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'bettingrelation') . '/bettingrelation.js');
  // uzel ještě neexistuje a vybral jsem rodiče
  if (!isset($form['nid']['#value']) && $form_state['values']['pnid']) {
    $parent_node = node_load($form_state['values']['pnid']);
    foreach ($parent_node->taxonomy as $value) {
      $taxonomy[$value->vid] = $value->tid;   
    }
    form_set_value($form['taxonomy'], $taxonomy, $form_state);
    form_set_value($form['game']['limit_min'], $parent_node->limit_min, $form_state);
    form_set_value($form['game']['limit_max'], $parent_node->limit_max, $form_state);
    form_set_value($form['game']['start'], betting_explode_date($parent_node->start), $form_state);
    // hodnoty nastavované jen pro klasickou hru - zde by se měl asi objevit nějaký hoočíček :)
    if ($form['type']['#value'] == 'game') {
      form_set_value($form['step'], 3, $form_state);
      // jelikož abonentí formulářový prvek neexistuje, musím data vložit takto :)
      $form_state['values']['abonent'] = $parent_node->abonent; 
    }
  }  
  // nevybral jsem rodiče - musím zkontrolovat zadané kategorie
  elseif (!$form_state['values']['pnid'] && $form_state['values']['taxonomy']) {
    // pokud nevyberu rodiče, musí být výběr kategorií povinný    
    $sport_vid = betting_get_vid('sport');
    $sort_vid = betting_get_vid('sort');    
    foreach ($form_state['values']['taxonomy'] as $key => $tid) {
      if (!$tid) {
        $name = ($key == $sport_vid) ? 'Sport' : 'Typ utkání';
        form_set_error('taxonomy]['.$key, t('!name field is required.', array('!name' => $name)));
      }
    }
  }
  // další podmíny na limity bodů
  elseif (!$form_state['values']['pnid'] ) {
    
  }
}

/**
* Vrátí seznam všech her, které ještě nezačaly.
* 
* @param mixed $sport
* @param mixed $sort
*/
function bettingrelation_get_games($edit = FALSE) {
  static $games;
  if (!$games) {
    $time = ($edit) ? '0' : time();
    $result = db_query("SELECT n.nid, n.title FROM node n INNER JOIN {betting_node} b ON b.nid = n.nid
      LEFT JOIN {bettingrelation} r ON n.nid = r.nid
      WHERE n.type = 'game' AND n.status = 1 AND b.start > %d AND r.pnid IS NULL ORDER BY n.title ASC", $time);
    while ($row = db_fetch_object($result)) {
      $games[$row->nid] = $row->title;
    }  
  }
  return (array)$games;
}

/**
* Modul Betting System a všechny jeho součásti jsou majetkem Petra Řezníčka (posta@petrreznicek.cz).
* Jakékoliv úpravy a další používání bez souhlasu autora budou považovány za porušení zákona č. 121/2000 Sb. 
* (C) 2009 Petr 'kecinzer' Řezníček
*/