<?php
/**
* (c) 2009 Petr 'kecinzer' Řezníček
*/

/**
* Přidání kurzu na aktuální ticket
* 
* @param mixed $cid
*/
function betting_add_to_ticket($cid) {
  // tady musím načíst NID současného kurzu a uložit do pole
  $node = db_fetch_object(db_query('SELECT nid FROM {betting_courses} WHERE cid = %d', $cid));
  global $user;
  $ticket = $user->ticket;
  $ticket[$node->nid] = $cid;
  user_save($user, array('ticket' => $ticket));
  drupal_set_message(t('Tip has been added to betting ticket.'));
  drupal_goto();
}

/**
* Funkce odkazující na formulář aktuálního tiketu a potvrzení vytvoření
* 
*/
function betting_current_ticket_route() {
  // pokud odesílám vsazenou částku na tiket
  if (($_POST['op'] == t('Submit ticket') && betting_check_points($_POST['points'])) || $_POST['form_id'] == 'betting_current_ticket_add_confirm') {
    $output = drupal_get_form('betting_current_ticket_add_confirm');
  }
  else {
    $output = drupal_get_form('betting_current_ticket');
  }
  return $output;
}

function betting_check_points($points, &$message = FALSE) {
  global $user;
  $user_points = _betting_load_user_points();
  $limit = db_fetch_object(db_query('SELECT MAX(bn.limit_min) AS min, MIN(bn.limit_max) AS max FROM {betting_courses} c LEFT JOIN {betting_node} bn ON bn.nid = c.nid WHERE c.cid IN (%s)', implode(',', $user->ticket)));
  if (!is_numeric($points)) {
    $message = t('Points has to be a integer.');
    return FALSE;
  }
  elseif ($user_points < $points) {
    $message = t('You do not have enough points for this bet.');
    return FALSE;
  }
  elseif ($points < $limit->min || $points > $limit->max) {
    $message = t('Your bet is out of allowed limit.');
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
* Generuje seznam aktuálně vybraných tipů a jejich kurzy.
* Uživatel má možnost libovolné tipy vymazaávat.
* Nakonec má možnost vsadit určitý počet bodů a odeslat.
* 
* @param mixed $form_state
*/
function betting_current_ticket($form_state) {
  global $user;
  // výběr provádím pouze pokud uživatel má vyplněné nějaké tipy
  if ($user->ticket) {    
    $result = db_query('
      SELECT DISTINCT n.nid, a.title AS abonent, r.eid, c.aid, c.cid, c.course, e.result, e.description, bn.limit_min, bn.limit_max
      FROM {betting_courses} c
      INNER JOIN {node} n ON n.nid = c.nid
      INNER JOIN {betting_eventuality} e ON e.eid = c.eid
      LEFT JOIN {node} a ON a.nid = c.aid
      LEFT JOIN {betting_node} bn ON bn.nid = n.nid
      LEFT JOIN {betting_results} r ON r.nid = n.nid
      WHERE r.nid IS NULL AND bn.start > UNIX_TIMESTAMP() AND c.cid IN (%s)
      ', implode(',', $user->ticket));
    while ($row = db_fetch_object($result)) {
      $node = node_load($row->nid);
      $ticket[] = array(
        'cid' => $row->cid,
        'title' => l($node->title, 'node/'.$row->nid),
        'abonent' => $row->abonent ? '<b>'. $row->result .'</b> (' . $row->description . ') for '. l($row->abonent, 'node/'.$row->aid) : '<b>' . $row->result . '</b> (' . $row->description . ')',
        'course' => $row->course,
        'min' => $row->limit_min,
        'max' => $row->limit_max,
      );
    }    
    // přidání dalších položek na ticket z jiných modulů
    $ticket = array_merge((array)$ticket, (array)module_invoke_all('active_ticket'));
    foreach ($user->ticket as $nid => $cid) {
      $found = FALSE;
      foreach ($ticket as $key => $item) {
        if ($item['cid'] == $cid) {
          $found = TRUE;
          $cids[$cid] = '';
          $form['title'][$cid] = array('#value' => $item['title']);
          $form['abonent'][$cid] = array('#value' => $item['abonent']);
          $form['course'][$cid] = array('#value' => $item['course']);
          $total_course[] = $item['course'];
          $min[] = $item['min'];
          $max[] = $item['max'];
          // zvyšuji rychlost aplikace ;) - to co jsem nalezl a zařidil vymažu
          unset($ticket[$key]);
        }
      }
      if (!$found) {
        // na tip již není možné si vsadit - musí se smazat
        $to_delete[] = $nid;
      }
    }
    // pokud nezískám žádná data, tak je ani nebudu zobrazovat
    if (is_array($cids)) {
      $form['total_course'] = array('#value' => round(array_product($total_course), 2));
      $form['cids'] = array(
        '#type' => 'checkboxes',
        '#options' => $cids
      );
      // políčko na vyplnění počtu bodů
      $form['points'] = array(
        '#type' => 'textfield',
        '#title' => t('Your bet'),
        '#description' => t('Number of points, that you will bet. Minimum is <b>@min</b> and maximum is <b>@max</b> points.', array('@min' => max($min), '@max' => min($max))),
        '#maxlength' => 6,
      );  
      // ještě nějaké buttony musím přidat také
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit ticket'),
      );
      $form['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete selected tips'),
      );
    }
    // vymažu uživatelská data
    _betting_delete_user_tips($to_delete); 
  }  
  return $form;
}

function theme_betting_current_ticket($form) {
  $header = array(
    t('Delete'),
    t('Title'),
    t('Bet'),
    t('Course'),
  );
  if (isset($form['title']) && is_array($form['title'])) {
    foreach (element_children($form['title']) as $key) {
      $rows[] = array(
        drupal_render($form['cids'][$key]),
        drupal_render($form['title'][$key]),
        drupal_render($form['abonent'][$key]),
        drupal_render($form['course'][$key]),
      );
    }
    $rows[] = array(
      array('data' => t('Total course:'), 'colspan' => '3'),
      '<b>'.drupal_render($form['total_course']).'</b>',
    );
  }
  else  {
    $rows[] = array(array('data' => t('You have no tips in current open ticket.'), 'colspan' => '4'));
  }  
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  
  return $output;
}

function betting_current_ticket_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete selected tips')) {
    $cids = array_filter($form_state['values']['cids']);
    if (count($cids) == 0) {
      form_set_error('', t('No items selected.'));
    }  
  }
  if ($form_state['clicked_button']['#value'] == t('Submit ticket')) {
    if (!betting_check_points($form_state['values']['points'], $message)) {
      form_set_error('points', $message);
    }
  }
}

/**
* Provedu vymazání tipů z lístku
* 
* @param mixed $form
* @param mixed $form_state
*/
function betting_current_ticket_submit($form, &$form_state) {
  global $user;
  $ticket = $user->ticket;
  $cids = array_filter($form_state['values']['cids']);
  foreach($cids as $cid) {
    foreach($ticket as $nid => $c) {
      if($c == $cid) {
        unset($ticket[$nid]);
      }
    }
  }
  user_save($user, array('ticket' => $ticket));
}

/**
* Potvrzovací formulář odeslání tiketu
* 
* @param mixed $form_state
*/
function betting_current_ticket_add_confirm(&$form_state) {
  // zde znovu zobrazím tabulku s výčetem typů a potvrzovací button ;)
  $edit = $form_state['post'];
  global $user;
  $header = array(
    t('Title'),
    t('Bet'),
    t('Course'),
  );
  // tady se musí vytvořit další hook ;)
  $result = db_query('
    SELECT DISTINCT n.nid, a.title AS abonent, c.aid, c.cid, c.course, e.result, e.description
    FROM {betting_courses} c
    INNER JOIN {node} n ON n.nid = c.nid
    INNER JOIN {betting_eventuality} e ON e.eid = c.eid
    LEFT JOIN {node} a ON a.nid = c.aid
    LEFT JOIN {betting_node} bn ON bn.nid = n.nid
    LEFT JOIN {betting_results} r ON r.nid = n.nid
    WHERE r.nid IS NULL AND bn.start > UNIX_TIMESTAMP() AND c.cid IN (%s)
    ', implode(',', $user->ticket));
  while ($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    $ticket[] = array(
      'cid' => $row->cid,
      'title' => l($node->title, 'node/'.$row->nid),
      'abonent' => $row->abonent ? '<b>'. $row->result .'</b> (' . $row->description . ') '. t('for') . ' ' . l($row->abonent, 'node/'.$row->aid) : '<b>' . $row->result . '</b> (' . $row->description . ')',
      'course' => $row->course,
    );
  }
  // deklarace dalšího hooku :)  
  $ticket = array_merge((array)$ticket, (array)module_invoke_all('active_ticket_confirm'));
  foreach ($user->ticket as $nid => $cid) {
    $found = FALSE;
    foreach ($ticket as $item) {
      if ($item['cid'] == $cid) {
        $found = TRUE;
        $rows[] = array(
          $item['title'],
          $item['abonent'],
          $item['course'],
        );
        $total_course[] = $item['course'];
      }
    }
    if (!$found) {
      // na tip již není možné si vsadit - musí se smazat
      $to_delete[] = $nid;
    }
  }
  _betting_delete_user_tips($to_delete);
  // součin kuzů
  $course = array_product($total_course);
  $rows[] = array(
    array('data' => t('Total course:'), 'colspan' => '2'),
    '<b>'.round($course, 2).'</b>',
  );
  $table = theme('table', $header, $rows);
  unset($rows);
  $header = array(
    t('Bet points'),
    t('Win points'),
  );
  $rows[] = array(
    $edit['points'],
    $edit['points'] * $course,
  );
  $table2 = theme('table', $header, $rows);
  $form['points'] = array('#type' => 'hidden', '#value' => $edit['points']);
  return confirm_form($form,
                      t('Do You realy want to submit this ticket?'),
                      'betting/current-ticket',
                      $table . $table2,
                      t('Submit'), t('Cancel'));
}

function betting_current_ticket_add_confirm_submit($form, &$form_state) {
  global $user;  
  if ($form_state['values']['confirm']) {
    // nejdříve vytvořím kontainer ticketu
    db_query('INSERT INTO {betting_ticket} (uid, points, time) VALUES (%d, %d, %d)', $user->uid, $form_state['values']['points'], time());
    // následně musím vložit jendotlivé tipy do ticketu
    $tid = db_last_insert_id('betting_ticket', 'tid');
    $i = 0;
    foreach ($user->ticket as $cid) {
      db_query('INSERT INTO {betting_tips} (tid, cid, sort) VALUES (%d, %d, %d)', $tid, $cid, $i);
      $i++;
    }
    // nastavím počet bodů
    $betting_points = _betting_load_user_points($user->uid);
    $user->betting_points = $betting_points - $form_state['values']['points'];
    // ještě smažu uživatelskou proměnnou
    user_save($user, array('ticket' => FALSE));
    drupal_set_message(t('The ticket hase been created.'));
  }
  $form_state['redirect'] = 'betting/tickets/' . $tid;
}

/**
* Vypisuje všechny již odeslané lístky
* Tohle bude tabulka ve které bude znázorněno dobrý, špatný, probíhající
* 
*/
function betting_tickets() {
  global $user;  
  $header = array(
    t('Ticket ID'),
    t('Send time'),
    t('Number of tips'),
    t('Bet points'),
    t('Possible win points'),
    t('Status'),
  );
  $result = db_query('SELECT t.tid, t.points, t.status, t.time, COUNT(ti.cid) AS tips FROM {betting_ticket} t INNER JOIN {betting_tips} ti ON t.tid = ti.tid WHERE uid = %d GROUP BY (t.tid) ORDER BY t.time DESC, t.tid DESC', $user->uid);
  while ($row = db_fetch_object($result)) {
    // v každém průchodu musím získat všechny kurzy daného lístku - strašný
    $rows[] = array(
      l($row->tid, 'betting/tickets/'.$row->tid),
      l(format_date($row->time, 'custom', 'd.m.Y H:i:s'), 'betting/tickets/'.$row->tid),
      $row->tips,
      $row->points,
      round(betting_product_course($row->tid) * $row->points),
      _betting_status($row->status),
    );
  }
  if(!$rows) {
    $rows[] = array(array('data' => t('You don\'t have any ticket yet.'), 'colspan' => '6'));
  }
  
  $output = theme('table', $header, $rows);
  return $output;
}

/**
* Zobrazení jenotlivých ticketů
* 
* @param mixed $tid
*/
function betting_ticket_show($T) {
  // v podstatě stejný výpis jako u aktuálního ticketu, jen se ještě budou zobrazovat statusy
  global $user;
  $header = array(
    t('Title'),
    t('Bet'),
    t('Status'),
    t('Course'),
  );  
  $result = db_query('
    SELECT n.nid, a.title AS abonent, c.aid, c.cid, c.course, e.result, e.description, t.status, t.tid, t.sort
    FROM {betting_courses} c
    INNER JOIN {node} n ON n.nid = c.nid
    INNER JOIN {betting_eventuality} e ON e.eid = c.eid
    LEFT JOIN {node} a ON a.nid = c.aid
    INNER JOIN {betting_tips} t ON t.cid = c.cid
    WHERE t.tid = %d
    ', $T->tid);
  while ($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    $ticket[] = array(
      'sort' => $row->sort,
      'title' => l($node->title, 'node/'.$row->nid),
      'eventuality' => $row->abonent ? '<b>'. $row->result .'</b> (' . $row->description . ') '.t('for').' '. l($row->abonent, 'node/'.$row->aid) : '<b>' . $row->result . '</b> (' . $row->description . ')',
      'status' => _betting_status($row->status),
      'course' => $row->course,
    );
  }
  // deklarace dalšího hooku :)  
  $ticket = array_merge((array)$ticket, (array)module_invoke_all('saved_ticket', $T));
  // teď musím nacpat data do řádků tabulky a seřadit je, oo yeah baby!!
  foreach ($ticket as $item) {
    $rows[$item['sort']] = array(
      $item['title'],
      $item['eventuality'],
      $item['status'],
      $item['course'],
    );
    $total_course[] = $item['course'];
  }
  // součin kuzů
  $course = array_product($total_course);
  $rows[] = array(
    array('data' => t('Total course:'), 'colspan' => '3'),
    '<b>'.round($course, 2).'</b>',
  );
  // seřadím řádky
  ksort($rows);
  $table = theme('table', $header, $rows);
  unset($rows);
  $header = array(
    t('Bet points'),
    t('Win points'),
    t('Overall Status'),
  );
  $rows[] = array(
    $T->points,
    round($T->points * $course),
    _betting_status($T->status),
  );
  $table2 = theme('table', $header, $rows);
  $output = $table . $table2;
  return $output;
}

/**
* Modul Betting System a všechny jeho součásti jsou majetkem Petra Řezníčka (posta@petrreznicek.cz).
* Jakékoliv úpravy a další používání bez souhlasu autora budou považovány za porušení zákona č. 121/2000 Sb. 
* (C) 2009 Petr 'kecinzer' Řezníček
*/