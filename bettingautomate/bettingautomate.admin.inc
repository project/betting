<?php
/**
* (c) 2009 Petr 'kecinzer' Řezníček
*/

/**
* Základní funkce pro aktivaci a deaktivaci automatického stahování
*/
function bettingautomate_admin($form_state) {
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable automate functions'),
    '#description' => t('Allows to checking for courses on game cration and also tries to find game results during cron runs.'),
    '#default_value' => variable_get('bettingautomate_enabled', 0),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function bettingautomate_admin_submit($form, &$form_state) {
  if ($form_state['values']['enabled']) {
    variable_set('bettingautomate_enabled', 1);
    drupal_set_message(t('Automated downloading have been enabled.'));
  }
  else {
    variable_del('bettingautomate_enabled');
    drupal_set_message(t('Automated downloading have been disabled.'));
  }
}

/**
* Funkce pro nastavení rozcestí pro formuláře.
* Vypsány jsou všechny potřebné funkce
*/

function bettingautomate_admin_websites($form_state) {
  if (isset($form_state['values']['delete']) && $form_state['values']['delete'] == t('Delete selected')) {
    return bettingautomate_admin_websites_confirm($form_state, array_filter($form_state['values']['wids']));
  }
  $form = bettingautomate_admin_websites_list();
  $form['#theme'] = 'bettingautomate_admin_websites_list';
  
  return $form;
}

function bettingautomate_admin_variables($form_state) {
  if (isset($form_state['values']['delete']) && $form_state['values']['delete'] == t('Delete selected')) {
    return bettingautomate_admin_variables_confirm($form_state, array_filter($form_state['values']['vids']));
  }
  $form = bettingautomate_admin_variables_list();
  $form['#theme'] = 'bettingautomate_admin_variables_list';
  
  return $form;
}

function bettingautomate_admin_maincategory($form_state) {
  if (isset($form_state['values']['delete']) && $form_state['values']['delete'] == t('Delete selected')) {
    return bettingautomate_admin_maincategory_confirm($form_state, array_filter($form_state['values']['mcids']));
  }
  $form = bettingautomate_admin_maincategory_list();
  $form['#theme'] = 'bettingautomate_admin_maincategory_list';
  
  return $form;
}

function bettingautomate_admin_subcategory($form_state) {
  if (isset($form_state['values']['delete']) && $form_state['values']['delete'] == t('Delete selected')) {
    return bettingautomate_admin_subcategory_confirm($form_state, array_filter($form_state['values']['scids']));
  }
  $form = bettingautomate_admin_subcategory_list();
  $form['#theme'] = 'bettingautomate_admin_subcategory_list';
  
  return $form;
}

function bettingautomate_admin_paths($form_state) {
  if (isset($form_state['values']['delete']) && $form_state['values']['delete'] == t('Delete selected')) {
    return bettingautomate_admin_paths_confirm($form_state, array_filter($form_state['values']['pids']));
  }
  $form = bettingautomate_admin_paths_list();
  $form['#theme'] = 'bettingautomate_admin_paths_list';
  
  return $form;
}

/**
* Validační funkce pro rozcestí.
*/

function bettingautomate_admin_websites_validate($form, &$form_state) {
  $wids = array_filter($form_state['values']['wids']);
  if (count($wids) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

function bettingautomate_admin_variables_validate($form, &$form_state) {
  $vids = array_filter($form_state['values']['vids']);
  if (count($vids) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

function bettingautomate_admin_maincategory_validate($form, &$form_state) {
  $mcids = array_filter($form_state['values']['mcids']);
  if (count($mcids) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

function bettingautomate_admin_subcategory_validate($form, &$form_state) {
  $scids = array_filter($form_state['values']['scids']);
  if (count($scids) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

function bettingautomate_admin_paths_validate($form, &$form_state) {
  $pids = array_filter($form_state['values']['pids']);
  if (count($pids) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

/**
* Odesílací funkce rozcestní - musím rebuildnout formulář, abych se dostal dál.
*/

function bettingautomate_admin_websites_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bettingautomate_admin_variables_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bettingautomate_admin_maincateogory_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bettingautomate_admin_subcategory_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bettingautomate_admin_paths_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
* Konfirmační formuláře pro potvrzení vymazání položek.
*/

function bettingautomate_admin_websites_confirm(&$form_state, $wids) {
  $form['wids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($wids as $wid => $value) {
    $name = db_result(db_query('SELECT name FROM {bettingautomate_web} WHERE wid = %d', $wid));
    $form['wids'][$wid] = array(
      '#type' => 'hidden',
      '#value' => $wid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingautomate_admin_websites_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/automate/websites', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function bettingautomate_admin_variables_confirm(&$form_state, $vids) {
  $form['vids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($vids as $vid => $value) {
    $name = db_result(db_query('SELECT name FROM {bettingautomate_variable} WHERE vid = %d', $vid));
    $form['vids'][$vid] = array(
      '#type' => 'hidden',
      '#value' => $vid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingautomate_admin_variables_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/automate/variables', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function bettingautomate_admin_maincategory_confirm(&$form_state, $vids) {
  $form['mcids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($mcids as $mcid => $value) {
    $name = db_result(db_query('SELECT name FROM {bettingautomate_main_category} WHERE mcid = %d', $mcid));
    $form['mcids'][$mcid] = array(
      '#type' => 'hidden',
      '#value' => $mcid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingautomate_admin_maincategory_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/automate/main-category', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function bettingautomate_admin_subcategory_confirm(&$form_state, $vids) {
  $form['mcids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($scids as $scid => $value) {
    $name = db_result(db_query('SELECT name FROM {bettingautomate_sub_category} WHERE scid = %d', $scid));
    $form['scids'][$scid] = array(
      '#type' => 'hidden',
      '#value' => $scid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingautomate_admin_subcategory_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/automate/sub-category', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function bettingautomate_admin_paths_confirm(&$form_state, $vids) {
  $form['mcids'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($pids as $pid => $value) {
    $name = db_result(db_query('SELECT value FROM {bettingautomate_path} WHERE pid = %d', $pid));
    $form['pids'][$pid] = array(
      '#type' => 'hidden',
      '#value' => $pid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($name) ."</li>\n",
    );
  }
  $form['delete'] = array('#type' => 'hidden', '#value' => t('Delete selected'));
  $form['#submit'][] = 'bettingautomate_admin_paths_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/betting/automate/paths', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

/**
* Po potvrzení vymažu záznamy.
*/

function bettingautomate_admin_websites_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['wids'] as $wid => $value) {
      db_query('DELETE FROM {bettingautomate_web} WHERE wid = %d', $wid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/websites';
  return;
}

function bettingautomate_admin_variables_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['vids'] as $vid => $value) {
      db_query('DELETE FROM {bettingautomate_variable} WHERE vid = %d', $vid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/variables';
  return;
}

function bettingautomate_admin_maincategory_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['mcids'] as $mcid => $value) {
      db_query('DELETE FROM {bettingautomate_main_category} WHERE mcid = %d', $mcid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/main-category';
  return;
}

function bettingautomate_admin_subcategory_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['scids'] as $scid => $value) {
      db_query('DELETE FROM {bettingautomate_sub_category} WHERE scid = %d', $scid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/sub-category';
  return;
}

function bettingautomate_admin_paths_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['pids'] as $pid => $value) {
      db_query('DELETE FROM {bettingautomate_path} WHERE pid = %d', $pid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/paths';
  return;
}

/**
* Výpisy všech hodnot v tabulkách s možností vybrat pár a smazat!
*/

function bettingautomate_admin_websites_list() {
  $result = db_query('SELECT * FROM {bettingautomate_web}');
  while ($row = db_fetch_object($result)) {
    $wids[$row->wid] = '';
    $form['name'][$row->wid] = array('#value' => $row->name);
    $form['description'][$row->wid] = array('#value' => $row->description);
    $form['status'][$row->wid] = array('#value' => $row->status);
    $form['edit'][$row->wid] = array('#value' => l(t('edit'), 'admin/betting/automate/websites/'.$row->wid));
  }
  if (is_array($wids)) {
    $form['wids'] = array(
      '#type' => 'checkboxes',
      '#options' => $wids,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  
  return $form;
}

function bettingautomate_admin_variables_list() {
  $result = db_query('SELECT * FROM {bettingautomate_variable}');
  while ($row = db_fetch_object($result)) {
    $vids[$row->vid] = '';
    $form['name'][$row->vid] = array('#value' => $row->name);
    $form['description'][$row->vid] = array('#value' => $row->description);
    $form['edit'][$row->vid] = array('#value' => l(t('edit'), 'admin/betting/automate/variables/'.$row->vid));
  }
  if (is_array($vids)) {
    $form['vids'] = array(
      '#type' => 'checkboxes',
      '#options' => $vids,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  
  return $form;
}

function bettingautomate_admin_maincategory_list() {
  $result = db_query('SELECT * FROM {bettingautomate_main_category}');
  while ($row = db_fetch_object($result)) {
    $mcids[$row->mcid] = '';
    $form['name'][$row->mcid] = array('#value' => $row->name);
    $form['description'][$row->mcid] = array('#value' => $row->description);
    $form['edit'][$row->mcid] = array('#value' => l(t('edit'), 'admin/betting/automate/main-category/'.$row->mcid));
  }
  if (is_array($mcids)) {
    $form['mcids'] = array(
      '#type' => 'checkboxes',
      '#options' => $mcids,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  
  return $form;
}

function bettingautomate_admin_subcategory_list() {
  $result = db_query('SELECT * FROM {bettingautomate_sub_category}');
  while ($row = db_fetch_object($result)) {
    $scids[$row->scid] = '';
    $form['name'][$row->scid] = array('#value' => $row->name);
    $form['description'][$row->scid] = array('#value' => $row->description);
    $form['edit'][$row->scid] = array('#value' => l(t('edit'), 'admin/betting/automate/main-category/'.$row->scid));
  }
  if (is_array($scids)) {
    $form['scids'] = array(
      '#type' => 'checkboxes',
      '#options' => $scids,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  
  return $form;
}

function bettingautomate_admin_paths_list() {
  // hlavička pro možnost řadit položky dle mého
  $header = array(
    array(),
    array('data' => t('Site'), 'field' => 'w.name', 'sort' => 'asc'),
    array('data' => t('Main C.'), 'field' => 'mc.name'),
    array('data' => t('Sub C.'), 'field' => 'sc.name'),
    array('data' => t('Variable'), 'field' => 'v.name'),
    t('Path'),
    t('Operations'),
  );
  $sql = 'SELECT w.name AS website, v.name AS variable, mc.name AS maincategory, sc.name AS subcategory, p.value AS path, p.pid
    FROM {bettingautomate_path} p
    INNER JOIN {bettingautomate_web} w ON w.wid = p.wid
    INNER JOIN {bettingautomate_variable} v ON v.vid = p.vid
    INNER JOIN {bettingautomate_main_category} mc ON mc.mcid = p.mcid
    INNER JOIN {bettingautomate_sub_category} sc ON sc.scid = p.scid';
  $sql .= tablesort_sql($header);
  $result = db_query($sql);
  while ($row = db_fetch_object($result)) {
    $pids[$row->pid] = '';
    $form['website'][$row->pid] = array('#value' => $row->website);
    $form['variable'][$row->pid] = array('#value' => $row->variable);
    $form['maincategory'][$row->pid] = array('#value' => $row->maincategory);
    $form['subcategory'][$row->pid] = array('#value' => $row->subcategory);
    $form['path'][$row->pid] = array('#value' => $row->path);
    $form['edit'][$row->pid] = array('#value' => l(t('edit'), 'admin/betting/automate/paths/'.$row->pid));
  }
  if (is_array($pids)) {
    $form['pids'] = array(
      '#type' => 'checkboxes',
      '#options' => $pids,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
  }
  
  return $form;
}

/**
* Theme funkce pro výpisy.
*/

function theme_bettingautomate_admin_websites_list(&$form) {
  $has_items = isset($form['name']) && is_array($form['name']);
  $select_header = $has_items ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Name'), t('Description'), t('Status'), t('Operations'));
  if ($has_items) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['wids'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['description'][$key]);
      $row[] = drupal_render($form['status'][$key]);
      $row[] = drupal_render($form['edit'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '5'));
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

function theme_bettingautomate_admin_variables_list(&$form) {
  $has_items = isset($form['name']) && is_array($form['name']);
  $select_header = $has_items ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Name'), t('Description'), t('Operations'));
  if ($has_items) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['vids'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['description'][$key]);
      $row[] = drupal_render($form['edit'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '4'));
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

function theme_bettingautomate_admin_maincategory_list(&$form) {
  $has_items = isset($form['name']) && is_array($form['name']);
  $select_header = $has_items ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Name'), t('Description'), t('Operations'));
  if ($has_items) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['mcids'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['description'][$key]);
      $row[] = drupal_render($form['edit'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '4'));
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

function theme_bettingautomate_admin_subcategory_list(&$form) {
  $has_items = isset($form['name']) && is_array($form['name']);
  $select_header = $has_items ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Name'), t('Description'), t('Operations'));
  if ($has_items) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['scids'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['description'][$key]);
      $row[] = drupal_render($form['edit'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '4'));
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

function theme_bettingautomate_admin_paths_list(&$form) {
  $has_items = isset($form['website']) && is_array($form['website']);
  $select_header = $has_items ? theme('table_select_header_cell') : '';
  $header = array(
    $select_header,
    array('data' => t('Site'), 'field' => 'w.name', 'sort' => 'asc'),
    array('data' => t('Main C.'), 'field' => 'mc.name'),
    array('data' => t('Sub C.'), 'field' => 'sc.name'),
    array('data' => t('Variable'), 'field' => 'v.name'),
    t('Path'),
    t('Operations'),
  );
  if ($has_items) {
    foreach (element_children($form['website']) as $key) {
      $row = array();
      $row[] = drupal_render($form['pids'][$key]);
      $row[] = drupal_render($form['website'][$key]);
      $row[] = drupal_render($form['maincategory'][$key]);
      $row[] = drupal_render($form['subcategory'][$key]);
      $row[] = drupal_render($form['variable'][$key]);
      $row[] = drupal_render($form['path'][$key]);
      $row[] = drupal_render($form['edit'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '7'));
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

function bettingautomate_admin_websites_form($form_state, $edit = NULL) {
  if ($edit->wid) {
    $form['wid'] = array(
      '#type' => 'hidden',
      '#value' => $edit->wid,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the website.'),
    '#default_value' => $edit->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit->description,
  );
  $form['url_courses'] = array(
    '#type' => 'textfield',
    '#title' => t('URL for courses'),
    '#description' => t('The site, where are courses located.'),
    '#default_value' => $edit->url_courses,
    '#required' => TRUE,
  );
  $form['url_results'] = array(
    '#type' => 'textfield',
    '#title' => t('URL for results'),
    '#description' => t('The site, where are results located.'),
    '#default_value' => $edit->url_results,
    '#required' => TRUE,
  );
  $form['format_match'] = array(
    '#type' => 'textfield',
    '#title' => t('Mach format'),
    '#description' => t('How is compose name of the match.'),
    '#default_value' => $edit->format_match,
    '#required' => TRUE,
  );
  $form['unfinished_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Unfinished value'),
    '#description' => t('What sign is for abonent that are not finished.'),
    '#default_value' => $edit->unfinished_value,
    '#required' => TRUE,
  );
  $form['date'] = array(
    '#type' => 'fieldset',
    '#title' => t('Datetime order'),
    '#description' => t('Defines, where are placed items in datetime value. If item is not presented, then set value to zero.'),
    '#tree' => TRUE,
  );
  $form['date']['day'] = array(
    '#type' => 'textfield',
    '#title' => t('Day'),
    '#description' => t('Position of day.'),
    '#default_value' => $edit->date_day,
    '#required' => TRUE,
  );
  $form['date']['month'] = array(
    '#type' => 'textfield',
    '#title' => t('Month'),
    '#description' => t('Position of month.'),
    '#default_value' => $edit->date_month,
    '#required' => TRUE,
  );
  $form['date']['year'] = array(
    '#type' => 'textfield',
    '#title' => t('Year'),
    '#description' => t('Position of year.'),
    '#default_value' => $edit->date_year,
    '#required' => TRUE,
  );
  $form['date']['hour'] = array(
    '#type' => 'textfield',
    '#title' => t('Hour'),
    '#description' => t('Position of hour.'),
    '#default_value' => $edit->date_hour,
    '#required' => TRUE,
  );
  $form['date']['minute'] = array(
    '#type' => 'textfield',
    '#title' => t('Minute'),
    '#description' => t('Position of minute.'),
    '#default_value' => $edit->date_minute,
    '#required' => TRUE,
  );  
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $edit->status,
    '#options' => array(
      '0' => t('Disabled'),
      '1' => t('Active'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function bettingautomate_admin_variables_form($form_state, $edit = NULL) {
  if ($edit->vid) {
    $form['vid'] = array(
      '#type' => 'hidden',
      '#value' => $edit->vid,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the website.'),
    '#default_value' => $edit->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit->description,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function bettingautomate_admin_maincategory_form($form_state, $edit = NULL) {
  if ($edit->mcid) {
    $form['mcid'] = array(
      '#type' => 'hidden',
      '#value' => $edit->mcid,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the maincategory.'),
    '#default_value' => $edit->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit->description,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function bettingautomate_admin_subcategory_form($form_state, $edit = NULL) {
  if ($edit->scid) {
    $form['scid'] = array(
      '#type' => 'hidden',
      '#value' => $edit->scid,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the subcategory.'),
    '#default_value' => $edit->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit->description,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function bettingautomate_admin_paths_form($form_state, $edit = NULL) {
  // musím načít dodatečné informace ;)
  $result = db_query('SELECT wid, name FROM {bettingautomate_web}');
  while ($row = db_fetch_object($result)) {
    $webistes[$row->wid] = $row->name;
  }
  $result = db_query('SELECT vid, name FROM {bettingautomate_variable}');
  while ($row = db_fetch_object($result)) {
    $variables[$row->vid] = $row->name;
  }
  $result = db_query('SELECT mcid, name FROM {bettingautomate_main_category}');
  while ($row = db_fetch_object($result)) {
    $maincategory[$row->mcid] = $row->name;
  }
  $result = db_query('SELECT scid, name FROM {bettingautomate_sub_category}');
  while ($row = db_fetch_object($result)) {
    $subcategory[$row->scid] = $row->name;
  }
  
  if ($edit->pid) {
    $form['pid'] = array(
      '#type' => 'hidden',
      '#value' => $edit->pid,
    );
  }
  $form['wid'] = array(
    '#type' => 'select',
    '#title' => t('Web site'),
    '#default_value' => $edit->wid,
    '#options' => array('' => t('- None selected -')) + $webistes,
    '#required' => TRUE,
  );
  $form['mcid'] = array(
    '#type' => 'select',
    '#title' => t('Main category'),
    '#default_value' => $edit->mcid,
    '#options' => array('' => t('- None selected -')) + $maincategory,
    '#required' => TRUE,
  );
  $form['scid'] = array(
    '#type' => 'select',
    '#title' => t('Sub category'),
    '#default_value' => $edit->scid,
    '#options' => array('' => t('- None selected -')) + $subcategory,
    '#required' => TRUE,
  );
  $form['vid'] = array(
    '#type' => 'select',
    '#title' => t('Variable'),
    '#default_value' => $edit->vid,
    '#options' => array('' => t('- None selected -')) + $variables,
    '#required' => TRUE,
  );  
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Path to selected variable.'),
    '#default_value' => $edit->value,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function bettingautomate_admin_websites_form_submit($form, &$form_state) {
  // pokud nastavuji status sajtě na 1, ostatním musím nastavit 0 :)
  if ($form_state['values']['status'] == 1) {
    db_query('UPDATE {bettingautomate_web} SET status = 0');
  }
  if ($form_state['values']['wid']) {
    db_query('UPDATE {bettingautomate_web} SET name = "%s", description = "%s", url_courses = "%s", url_results = "%s", format_match = "%s", unfinished_value = "%s", date_day = %d, date_month = %d, date_year = %d, date_hour = %d, date_minute = %d, status = %d
      WHERE wid = %d', $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['url_courses'], $form_state['values']['url_results'], $form_state['values']['format_match'], $form_state['values']['unfinished_value'], $form_state['values']['date']['day'], $form_state['values']['date']['month'], $form_state['values']['date']['year'], $form_state['values']['date']['hour'], $form_state['values']['date']['minute'], $form_state['values']['status'], $form_state['values']['wid']);
    drupal_set_message(t('Website have been updated.'));
  }
  else {
    db_query('INSERT INTO {bettingautomate_web} (name, description, url_courses, url_results, format_match, unfinished_value, date_day, date_month, date_year, date_hour, date_minute, status)
      VALUES ("%s", "%s", "%s", "%s", "%s", "%s", %d, %d, %d, %d, %d, %d)'
      , $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['url_courses'], $form_state['values']['url_results'], $form_state['values']['format_match'], $form_state['values']['unfinished_value'], $form_state['values']['date']['day'], $form_state['values']['date']['month'], $form_state['values']['date']['year'], $form_state['values']['date']['hour'], $form_state['values']['date']['minute'], $form_state['values']['status']);
    drupal_set_message(t('Website have been created.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/websites';
  return;
}

function bettingautomate_admin_variables_form_submit($form, &$form_state) {
  if ($form_state['values']['vid']) {
    db_query('UPDATE {bettingautomate_variable} SET name = "%s", description = "%s" WHERE vid = %d', $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['vid']);
    drupal_set_message(t('Variable have been updated.'));
  }
  else {
    db_query('INSERT INTO {bettingautomate_variable} (name, description) VALUES ("%s", "%s")', $form_state['values']['name'], $form_state['values']['description']);
    drupal_set_message(t('Variable have been created.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/variables';
  return;
}

function bettingautomate_admin_maincategory_form_submit($form, &$form_state) {
  if ($form_state['values']['mcid']) {
    db_query('UPDATE {bettingautomate_main_category} SET name = "%s", description = "%s" WHERE mcid = %d', $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['mcid']);
    drupal_set_message(t('Main category have been updated.'));
  }
  else {
    db_query('INSERT INTO {bettingautomate_main_category} (name, description) VALUES ("%s", "%s")', $form_state['values']['name'], $form_state['values']['description']);
    drupal_set_message(t('Variable have been created.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/main-category';
  return;
}

function bettingautomate_admin_subcategory_form_submit($form, &$form_state) {
  if ($form_state['values']['scid']) {
    db_query('UPDATE {bettingautomate_sub_category} SET name = "%s", description = "%s" WHERE scid = %d', $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['scid']);
    drupal_set_message(t('Sub category have been updated.'));
  }
  else {
    db_query('INSERT INTO {bettingautomate_sub_category} (name, description) VALUES ("%s", "%s")', $form_state['values']['name'], $form_state['values']['description']);
    drupal_set_message(t('Sub category have been created.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/sub-category';
  return;
}

function bettingautomate_admin_paths_form_submit($form, &$form_state) {
  if ($form_state['values']['pid']) {
    db_query('UPDATE {bettingautomate_path} SET value = "%s", wid = %d, vid = %d, mcid = %d, scid = %d WHERE pid = %d', $form_state['values']['path'], $form_state['values']['wid'], $form_state['values']['vid'], $form_state['values']['mcid'], $form_state['values']['scid'], $form_state['values']['pid']);
    drupal_set_message(t('Path have been updated.'));
  }
  else {
    db_query('INSERT INTO {bettingautomate_path} (wid, vid, mcid, scid, value) VALUES (%d, %d, %d, %d, "%s")', $form_state['values']['wid'], $form_state['values']['vid'], $form_state['values']['mcid'], $form_state['values']['scid'], $form_state['values']['path']);
    drupal_set_message(t('Path have been created.'));
  }
  $form_state['redirect'] = 'admin/betting/automate/paths';
  return;
}

/**
* Modul Betting System a všechny jeho součásti jsou majetkem Petra Řezníčka (posta@petrreznicek.cz).
* Jakékoliv úpravy a další používání bez souhlasu autora budou považovány za porušení zákona č. 121/2000 Sb. 
* (C) 2009 Petr 'kecinzer' Řezníček
*/