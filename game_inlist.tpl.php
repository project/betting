<?php
// popisek
?>
<div class="game clear-block">
  <h3><?php print $title ?></h3>
  <div class="meta">  
    <div class="taxonomy"><?php print $terms ?></div>
  </div>
  <div class="content">
    <?php print $content ?>
  </div>
</div>